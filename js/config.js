app.config(function($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: 'pages/homePage.html'
    }).otherwise({
        redirectTo: '/'
    })
});

app.config(['$qProvider', function($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app.run(['$anchorScroll', function($anchorScroll) {
    $anchorScroll = angular.noop;
}])