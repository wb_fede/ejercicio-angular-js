/****************** APP Module *****************/
var app = angular.module('ejercicioApp', ['datatables', 'chart.js', 'ngRoute']).config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.
        'https://www.webbuilders.com.ar/**'
    ]);
});

/****************** Services  *****************/
app.service('dataService', function($http, $q) {
    var jsonData = "";
    return {
        getJson: function(formData) {
            //format date
            var ini = new Date(formData.ini);
            var iniString = ("0" + (ini.getMonth() + 1)).slice(-2) + "/" + ("0" + ini.getDate()).slice(-2) + "/" + +ini.getFullYear();
            var fin = new Date(formData.ini);
            var finString = ("0" + (fin.getMonth() + 1)).slice(-2) + "/" + ("0" + fin.getDate()).slice(-2) + "/" + +fin.getFullYear();
            //request
            var defer = $q.defer();
            $http.get('https://www.webbuilders.com.ar/test/data.php').then(function(result) {
                defer.resolve(result.data);
            }, function(error) {
                console.log("error");
                return defer.reject();
            });
            return defer.promise;
        },

        setData: function(str) {
            jsonData = str;
        },

        getData: function() {
            return jsonData;
        }
    }
});
/****************** APP Controllers *****************/
app.controller('mainCtrl', mainController);
app.controller('tableCtrl', tableController);

// Main controller
function mainController($scope, $http) {

    // paths
    $scope.navbarPath = 'components/navbar.html';
    $scope.tablePath = 'components/table.html';
    $scope.homePath = 'pages/homePage.html';



};

// Table controller
function tableController(DTOptionsBuilder, DTColumnBuilder, dataService, $scope) {
    var vm = this;


    var dataTable;

    //flags
    vm.showtable = true;
    vm.showMenu = false;
    vm.errorMessage = null;

    // Datatable properties

    vm.dtColumns = [
        DTColumnBuilder.newColumn('dia').withTitle('Día'),
        DTColumnBuilder.newColumn('tiempo').withTitle('Tiempo (hs)'),
        DTColumnBuilder.newColumn('distancia').withTitle('Distancia (kms)')
    ];
    vm.dtInstance = {};
    vm.dtOptions = DTOptionsBuilder.fromFnPromise('');
    vm.dtOptions = DTOptionsBuilder.newOptions().withLanguageSource('translation/spanish.json');
    // get data from service
    $scope.obtenerData = function(form) {
        if (!form.$valid) {
            return;
        }
        dataService.getJson(form).then(function(result) {
            // call service
            vm.dtOptions = DTOptionsBuilder.fromFnPromise(dataService.getJson(form));
            //Translation
            //vm.dtOptions.withLanguageSource('translation/spanish.json');
            //if table -> render
            if (vm.showTable) {
                vm.dtInstance._renderer.rerender();
            }

            //generate chart
            dataTable = dataService.getJson(form);
            // show table
            vm.showtable = true;
            vm.showMenu = true;
        }, function(error) {
            vm.errorMessage = "Ocurrió un error al obtener los datos";
            return null;
        });;


    };

    // generate chart
    $scope.generateChart = function() {

        //hide table
        vm.showtable = false;
        //get data from service
        $scope.rslt = dataTable.then(function(dataArray) {
            var labels = [];
            var data = [];
            angular.forEach(dataArray, function(value, key) {
                labels.push('Día ' + value['dia']);
                data.push(value['distancia']);
            });
            //generate chart
            $scope.series = ["Distancia (Kms)"];
            $scope.labels = labels;
            $scope.chartData = data;
            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
            $scope.options = {
                scales: {
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                        labelString: 'Distancia'
                    }]
                }
            };
        }, function(error) {
            vm.errorMessage = "Ocurrió un error al obtener el gráfico";
        });

    }
}