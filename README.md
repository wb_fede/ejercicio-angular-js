# Ejercicio Angular Js

## Funcionamiento

Esta aplicación web obtiene un JSON desde el sitio https://www.webbuilders.com.ar/test/data.php y lo muestra en un DataTable, además de generar un gráfico con la información. 

## Tecnologías Utilizadas

Se ha utilizado
	- Angular Js
	- Boostrap
	- Jquery
	- Html / Css / JS

## Autor

Devoto Federico